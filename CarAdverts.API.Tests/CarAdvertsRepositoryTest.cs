﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CarAdverts.API.Models;
using CarAdverts.API.Models.DataManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarAdverts.API.Tests
{
    [TestClass]
    public class CarAdvertsRepositoryTest
    {
        [TestMethod]
        public void GetAllAdvertsOrderByTitleTest()
        {
            List<CarAdvert> carAdverts = MockRrpositoryHelper.GetAllCarAdverts();

            var mockSet = MockRrpositoryHelper.MockDBSet(carAdverts);

            var mockContext = new Mock<CarAdvertsContext>();
            mockContext.Setup(c => c.CarAdverts).Returns(mockSet.Object);

            // Get All car adverts from repository
            var repository = new CarAdvertsDataManager(mockContext.Object);
            var actual = repository.GetAll("title");

            // Assert
            // Ensure that 4 adverts are returned and
            // the first one's title is "Audi A4 Avant"
            Assert.AreEqual(4, actual.Count());
            Assert.AreEqual("Audi A4 Avant", actual.First().Title);
        }

        [TestMethod]
        public void CreateCarAdvertsTest()
        {
            // mock DbSet and DbContext
            var mockSet = new Mock<DbSet<CarAdvert>>();

            var mockContext = new Mock<CarAdvertsContext>();
            mockContext.Setup(c => c.CarAdverts).Returns(mockSet.Object);

            //Add new adverts
            var repository = new CarAdvertsDataManager(mockContext.Object);
            repository.Add(new CarAdvert() { Title = "Kia Ceed", Fuel = FuelType.Diesel, IsNew = true, Price = 19650 });

            // Assert
            // Verifies that a advertesment was added and save changes
            mockSet.Verify(m => m.Add(It.IsAny<CarAdvert>()), Times.Once);
            mockContext.Verify(m => m.SaveChanges(), Times.Once);
        }

        [TestMethod]
        public void GetByIdCarAdvertsTest()
        {
            List<CarAdvert> carAdverts = MockRrpositoryHelper.GetAllCarAdverts();
            // mock DbSet and DbContext
            var mockSet = MockRrpositoryHelper.MockDBSet(carAdverts);

            var mockContext = new Mock<CarAdvertsContext>();
            mockContext.Setup(c => c.CarAdverts).Returns(mockSet.Object);

            //Get new advertesment for id ==1
            var repository = new CarAdvertsDataManager(mockContext.Object);
            var actual = repository.Get(1);

            // Assert
            // Ensure that get car adverts
            // with id 1
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Id);
        }

        [TestMethod]
        public void UpdateCarAdvertsTest()
        {
            List<CarAdvert> carAdverts = MockRrpositoryHelper.GetAllCarAdverts();
            // mock DbSet and DbContext
            var mockSet = MockRrpositoryHelper.MockDBSet(carAdverts);

            var mockContext = new Mock<CarAdvertsContext>();
            mockContext.Setup(c => c.CarAdverts).Returns(mockSet.Object);

            //Add new adverts
            var repository = new CarAdvertsDataManager(mockContext.Object);
            var actual1 = repository.Get(1);
            var id = repository.Update(1, new CarAdvert() { Id = 1, Title = "Audi RS4", Fuel = FuelType.Diesel, IsNew = true, Price = 72000});

            mockContext.Verify(m => m.SaveChanges(), Times.Once);

            var actual = repository.Get(id);
            // Assert
            // Ensure that car advertesment was updated to a new car title and 
            
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.IsNew);
            Assert.AreEqual(1, actual.Id);
            Assert.AreEqual("Audi RS4", actual.Title);
        }

        [TestMethod]
        public void DeleteCarAdvertsTest()
        {
            List<CarAdvert> carAdverts = MockRrpositoryHelper.GetAllCarAdverts();
            // mock DbSet and DbContext
            var mockSet = MockRrpositoryHelper.MockDBSet(carAdverts);

            var mockContext = new Mock<CarAdvertsContext>();
            mockContext.Setup(c => c.CarAdverts).Returns(mockSet.Object);

            //Add new adverts
            var repository = new CarAdvertsDataManager(mockContext.Object);
            int idToDelete = 1;
            var carAdv = repository.Delete(idToDelete);

            var actual = repository.GetAll("id");
            // Assert
            // Ensure that car advertesment was deleted 

            Assert.AreEqual(3, actual.Count());
            Assert.AreEqual("BMW X5", actual.First().Title);

            mockSet.Verify(m => m.Remove(It.IsAny<CarAdvert>()), Times.Once);
            mockContext.Verify(m => m.SaveChanges(), Times.Once);
        }

    }
}
