﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CarAdverts.API.Models;
using Moq;
using System.Data.Entity;
using System.Linq;
using CarAdverts.API.Models.DataManager;
using CarAdverts.API.Models.Repository;
using CarAdverts.API.Controllers;
using log4net;
using System.Web.Http.Results;
using System.Net;

namespace CarAdverts.API.Tests
{
    [TestClass]
    public class CarAdvertsControllerTest
    {       
     
        [TestMethod]
        public void CreateAdvert_ShouldReturnTheSameAdvert()
        {
            var mockRepo = MockRepository();
            var logger = new Mock<ILog>();
            var controller = new CarAdvertsController(mockRepo, logger.Object);

            var item = GetTestAdvert();

            var result =  controller.AddCarAdverts(item) as OkNegotiatedContentResult<int>;

            Assert.IsNotNull(result);
        }



        [TestMethod]
        public void UpdateAdvert_ShouldReturnStatusCode()
        {
            var mockRepo = MockRepository();
            var logger = new Mock<ILog>();
            var controller = new CarAdvertsController(mockRepo, logger.Object);


            var item = GetTestAdvert();

            var result = controller.AddCarAdverts(item) as OkNegotiatedContentResult<int>;
            
            Assert.AreEqual(item.Id, result.Content);
        }

        [TestMethod]
        public void UpdateAdvert_ShouldFail_WhenNullObject()
        {
            var mockRepo = MockRepository();
            var logger = new Mock<ILog>();
            var controller = new CarAdvertsController(mockRepo, logger.Object);

            var badresult = controller.Update(9999, null);
            Assert.IsInstanceOfType(badresult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void UpdateAdvert_ShouldFail_WhenIdIsWrong()
        {
            var mockRepo = MockRepository();
            var logger = new Mock<ILog>();
            var controller = new CarAdvertsController(mockRepo, logger.Object);

            var badresult = controller.Update(9999, GetTestAdvert());
            Assert.IsInstanceOfType(badresult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void GetAdvert_ShouldReturnAdvertWithSameId()
        {
            var mockRepo = MockRepository();
            var logger = new Mock<ILog>();
            var controller = new CarAdvertsController(mockRepo, logger.Object);

            int id = 3;
            var result = controller.Get(id) as OkNegotiatedContentResult<CarAdvert>;

            Assert.IsNotNull(result);
            Assert.AreEqual(id, result.Content.Id);
        }

        [TestMethod]
        public void GetAdverts_ShouldReturnAllAdverts()
        {
            var mockRepo = MockRepository();
            var logger = new Mock<ILog>();
            var controller = new CarAdvertsController(mockRepo, logger.Object);
                        
            var result = controller.GetAllCarAdverts() as OkNegotiatedContentResult<IEnumerable<CarAdvert>>;

            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Content.ToList().Count);
        }

        [TestMethod]
        public void DeleteAdvert_ShouldReturnOK()
        {
            var mockRepo = MockRepository();
            var logger = new Mock<ILog>();
            var controller = new CarAdvertsController(mockRepo, logger.Object);
            int deleteId = 3;
            var item = mockRepo.Get(deleteId);
            var result = controller.RemoveCarAdverts(deleteId) as OkNegotiatedContentResult<CarAdvert>;

            Assert.IsNotNull(result);
            Assert.AreEqual(item.Id, result.Content.Id);
        }


        private IDataRepository<CarAdvert, int> MockRepository()
        {
            List<CarAdvert> carAdverts = MockRrpositoryHelper.GetAllCarAdverts();

            var mockSet = MockRrpositoryHelper.MockDBSet(carAdverts);

            var mockContext = new Mock<CarAdvertsContext>();
            mockContext.Setup(c => c.CarAdverts).Returns(mockSet.Object);

            return new CarAdvertsDataManager(mockContext.Object); ;
        }
            
        
        private CarAdvert GetTestAdvert()
        {
            return new CarAdvert() { Id = 100, Title = "BMW 5", Fuel = FuelType.Diesel, IsNew = false, Mileage = 75300, Price = 25620, RegistrationDate = DateTime.ParseExact("2016-05-24", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) };
        }
    }
}
