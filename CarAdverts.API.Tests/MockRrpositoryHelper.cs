﻿using CarAdverts.API.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarAdverts.API.Tests
{
    public class MockRrpositoryHelper
    {
        public static List<CarAdvert> GetAllCarAdverts()
        {
            var carAdverts = new List<CarAdvert>()
             {
                 new CarAdvert() {Id = 0, Title = "BMW X5", Fuel = FuelType.Diesel, IsNew = false, Mileage = 75300, Price = 25620, RegistrationDate = DateTime.ParseExact("2016-05-24", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) },
                 new CarAdvert() {Id = 1, Title = "Audi A4 Avant", Fuel = FuelType.Gasoline, IsNew = false, Mileage = 52000, Price = 22000, RegistrationDate = DateTime.ParseExact("2015-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) },
                 new CarAdvert() {Id = 2, Title = "Mercedes-Benz E200", Fuel = FuelType.Diesel, IsNew = false, Mileage = 175300, Price = 19260, RegistrationDate = DateTime.ParseExact("2016-05-24", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) },
                 new CarAdvert() {Id = 3, Title = "Audi A5", Fuel = FuelType.Gasoline, IsNew = true, Mileage = 0, Price = 22000, RegistrationDate = new DateTime() }
             };

            return carAdverts;
        }
        public static Mock<DbSet<CarAdvert>> MockDBSet(List<CarAdvert> carAdverts)
        {
            var mockSet = new Mock<DbSet<CarAdvert>>();
            mockSet.As<IQueryable<CarAdvert>>().Setup(m => m.Provider).Returns(carAdverts.AsQueryable().Provider);
            mockSet.As<IQueryable<CarAdvert>>().Setup(m => m.Expression).Returns(carAdverts.AsQueryable().Expression);
            mockSet.As<IQueryable<CarAdvert>>().Setup(m => m.ElementType).Returns(carAdverts.AsQueryable().ElementType);
            mockSet.As<IQueryable<CarAdvert>>().Setup(m => m.GetEnumerator()).Returns(carAdverts.GetEnumerator());
            mockSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => carAdverts.FirstOrDefault(d => d.Id == (int)ids[0]));
            mockSet.Setup(m => m.Remove(It.IsAny<CarAdvert>())).Callback<CarAdvert>((entity) => carAdverts.Remove(entity));

            return mockSet;
        }

    }
}
