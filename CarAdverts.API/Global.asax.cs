﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.WebApi;
using CarAdverts.API.Models;
using CarAdverts.API.Models.DataManager;
using CarAdverts.API.Models.Repository;
using log4net;

namespace CarAdverts.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            log4net.Config.XmlConfigurator.Configure();

            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            //builder.RegisterType<CarAdvertsContext>().As<DbContext>();
            builder.RegisterType<CarAdvertsContext>().InstancePerLifetimeScope();
            builder.RegisterType<CarAdvertsDataManager>().As<IDataRepository<CarAdvert, int>>().InstancePerRequest();
            //builder.RegisterGeneric(typeof(CarAdvertsDataManager)).As(typeof(IDataRepository<CarAdvert, int>));
            builder.Register(c => log4net.LogManager.GetLogger(typeof(Object))).As<ILog>();

            var container = builder.Build();

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container); // webapi integration
        }
    }
}
