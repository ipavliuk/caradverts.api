﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarAdverts.API.Models.Repository
{
    public interface IDataRepository<TEntiry, U> where TEntiry : class
    {
        IEnumerable<TEntiry> GetAll(string orderBy);
        int Add(TEntiry entity);
        TEntiry Get(U id);
        int Update(U id, TEntiry entity);
        TEntiry Delete(U id);
    }
}
