﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CarAdverts.API.Models
{
    public class CarAdvertsContext : DbContext
    {
        public CarAdvertsContext(): base("name=CarAdvertsConnectionString") 
        {
        }

        public virtual DbSet<CarAdvert> CarAdverts { get; set; }


    }
}