﻿using CarAdverts.API.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarAdverts.API.Extensions;

namespace CarAdverts.API.Models.DataManager
{
    public class CarAdvertsDataManager : IDataRepository<CarAdvert, int>
    {
        private readonly CarAdvertsContext _context;
        public CarAdvertsDataManager(CarAdvertsContext context)
        {
            _context = context;
        }

        public int Add(CarAdvert entity)
        {
            _context.CarAdverts.Add(entity);
            _context.SaveChanges();
            int id = entity.Id;
            return id;
        }

        public CarAdvert Delete(int id)
        {
            var carAdv = _context.CarAdverts.FirstOrDefault(e => e.Id == id);
            if(carAdv != null)
            {
                _context.CarAdverts.Remove(carAdv);
                _context.SaveChanges();
            }
            return carAdv;
        }

        public CarAdvert Get(int id)
        {
            return _context.CarAdverts.FirstOrDefault(e => e.Id == id);
        }

        public IEnumerable<CarAdvert> GetAll(string orderBy)
        {
            return _context.CarAdverts.OrderBy(orderBy).ToList();
        }

        public int Update(int id, CarAdvert entity)
        {
            var carAdv = _context.CarAdverts.Find(id);
            if(carAdv != null)
            {
                carAdv.Title = entity.Title;
                carAdv.Fuel = entity.Fuel;
                carAdv.IsNew = entity.IsNew;
                carAdv.Mileage = entity.Mileage;
                carAdv.Price = entity.Price;
                carAdv.RegistrationDate = entity.RegistrationDate;
                _context.SaveChanges();
            }

            return id;
        }
    }
}