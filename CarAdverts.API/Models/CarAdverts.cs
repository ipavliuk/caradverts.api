﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarAdverts.API.Models
{
	public class CarAdvert
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

        [Required]
		public string Title { get; set; }
        [Required]
        public FuelType Fuel { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public bool IsNew { get; set; }

        public int Mileage { get; set; }

        public DateTime RegistrationDate { get; set; }



    }
}