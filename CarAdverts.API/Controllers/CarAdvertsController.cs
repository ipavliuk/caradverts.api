﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CarAdverts.API.Models;
using CarAdverts.API.Models.Repository;
using log4net;
using Newtonsoft.Json;

namespace CarAdverts.API.Controllers
{
    public class CarAdvertsController : ApiController
    {
        private readonly IDataRepository<CarAdvert, int> _repo;
        private readonly ILog _logger;
        public CarAdvertsController(IDataRepository<CarAdvert, int> repo, ILog logger)
        {
            _repo = repo;
            _logger = logger;
        }

        [HttpGet]
        [Route("v1/adverts")]
        public IHttpActionResult GetAllCarAdverts(string orderBy = "id")
        {
            try
            {
                _logger.Info($"get all car adverts ordered by {orderBy} field");

                return Ok(_repo.GetAll(orderBy)); 
            }
            catch (Exception ex)
            {
                _logger.Error($"Exception during get all car adverts; ex = {ex}");
                return Content(HttpStatusCode.InternalServerError, "System Error");
            }
        }

        
        [HttpGet]
        [Route("v1/adverts/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                _logger.Info($"get car advert request id = {id}");

                if (id <= 0)
                {
                    return BadRequest();
                }

                var carAdverts = _repo.Get(id);


                return carAdverts != null ? (IHttpActionResult)Ok(carAdverts) : (IHttpActionResult)NotFound();
            }
            catch (Exception ex)
            {
                _logger.Error($"Exception during get all car advert; ex = {ex}");
                return Content(HttpStatusCode.InternalServerError, "System Error");
            }
            
        }

        // POST: v1/adverts
        [HttpPost]
        [Route("v1/adverts")]
        public IHttpActionResult AddCarAdverts([FromBody]CarAdvert adverts)
        {
            try
            {
                _logger.Info($"Add CarAdverts request...");
                if(adverts == null)
                {
                    return BadRequest();
                }

                return Ok(_repo.Add(adverts));
            }
            catch (Exception ex)
            {
                _logger.Error($"Exception during add car advert; ex = {ex}");
                return Content(HttpStatusCode.InternalServerError, "System Error");
            }

        }

        // PUT: v1/adverts/:id
        [HttpPut]
        [Route("v1/adverts/{id}")]
        public IHttpActionResult Update(int id, [FromBody]CarAdvert entity)
        {
            try
            {
                _logger.Info($"Update CarAdverts request... ");
                if (entity == null || entity?.Id != id)
                {
                    return BadRequest();
                }

                int nid = _repo.Update(id, entity);
                return Ok(nid);
            }
            catch (Exception ex)
            {
                _logger.Error($"Exception during get all car advert; ex = {ex}");
                return Content(HttpStatusCode.InternalServerError, "System Error");
            }
        }

        // DELETE: api/adverts/:id
        [HttpDelete]
        [Route("v1/adverts/{id}")]
        public IHttpActionResult RemoveCarAdverts(int id)
        {
            try
            {
                _logger.Info($"Remove CarAdverts - request id = {id}");

                if (id <= 0)
                {
                    return BadRequest();
                }

                return Ok(_repo.Delete(id));
            }
            catch (Exception ex)
            {
                _logger.Error($"Exception during remove car advert; ex = {ex}");
                return Content(HttpStatusCode.InternalServerError, "System Error");
            }
            
        }

    }
}
