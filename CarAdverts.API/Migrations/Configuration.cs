
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using CarAdverts.API.Models;
//using CarAdvertsModel = CarAdverts.API.Models;

namespace CarAdverts.API.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CarAdvertsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CarAdvertsContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            if (!context.CarAdverts.Any())
            {
                var carAdverts = new List<CarAdvert>()
                    {
                        new CarAdvert() { Title = "BMW 530i", Fuel = FuelType.Gasoline, IsNew = false, Mileage = 75300, Price = 25620, RegistrationDate = DateTime.ParseExact("2014-05-24", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) },
                        new CarAdvert() { Title = "Audi A4 Avant", Fuel = FuelType.Gasoline, IsNew = false, Mileage = 158300, Price = 22000, RegistrationDate = DateTime.ParseExact("2015-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) }
                    };

                context.CarAdverts.AddRange(carAdverts);
                context.SaveChanges();
                base.Seed(context);
            }

        }
    }
}
