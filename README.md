# Car Adverts REST API

### Frameworks
    - ASP.NET Web API 2
    - Entity Framework 6
    - Microsoft  SQL Server 2017 Express Edition
	- Autofac 
	- log4net	
    
### Installation
1. Create Database **CarAdverts** using Sql Server Management Studio by executing the following script:
    CREATE DATABASE CarAdverts;
2. Update ConnectionString *CarAdvertsConnectionString*  in the project's **CarAdverts.API** - **web.config** to appropriate server and user credentials.
3. Execute Database Migration in the Package Manager Console: **Tools –> Library Package Manager –> Package Manager Console**
    * Run the Enable-Migrations command in Package Manager Console
    * Add-Migration CarAdvets
    * Update-Database --Verbose
 

### Car Advert Data Structure
| Field | Type | Mandatory |
| ------ | ------ |------ |
| Id | int | yes |
| Title | string | yes | 
| Fuel | int : enum {gasoline = 1, diesel = 2} | yes |  
| Price | decimal | yes | 
| IsNew | bool | yes |  
| Mileage | int | no |
| RegistrationDate | DateTime | no | 


 
### Api design:
| Action | HTTP method | URI |
| ------ | ------ |------ |
| List of all car adverts | GET | **/v1/adverts** |
| Sorted by  Title | GET | **/v1/adverts?sort=title** |
| Get car advert by id | GET | **/v1/adverts/{id}** |
| Add new car advert | POST | **/v1/adverts** |
| Update car advert | PUT | **/v1/adverts/{id}** |
| Delete car advert | DELETE | **/v1/adverts/{id}** | 

### Integration Testing
TestScript directory contains Postman script for import with presefined RESTAPI calls.